import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DataTableModule } from "angular2-datatable";
import { SimpleNotificationsModule } from 'angular2-notifications';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { LoadersCssModule } from 'angular2-loaders-css';
import { ChartsModule } from 'ng2-charts';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { Ng2MapModule} from 'ng2-map';

import { UsuarioRoutingModule } from './usuario.routing';

import { UsersService } from "./../admin/_services/users.service";
import { RolesService } from "./../admin/_services/roles.service";
import { PuestosService } from "./../admin/_services/puestos.service";
import { ModulosService } from "./../admin/_services/modulos.service";
import { AccesosService } from "./../admin/_services/accesos.service";
import { AlertasService } from "./../admin/_services/alertas.service";
import { EventosService } from "./../admin/_services/eventos.service";
import { EventosUsuariosService } from "./../admin/_services/eventos-usuarios.service";
import { UbicacionesService } from "./../admin/_services/ubicaciones.service";

import { UsuarioComponent } from './usuario.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoaderComponent } from './loader/loader.component';
import { PerfilComponent } from './perfil/perfil.component';
import { UbicacionesComponent } from './ubicaciones/ubicaciones.component';
import { AlertasComponent } from './alertas/alertas.component';
import { AlertasAtendidasComponent } from './alertas-atendidas/alertas-atendidas.component';
import { EventosComponent } from './eventos/eventos.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ChartsModule,
    SimpleNotificationsModule.forRoot(),
    Ng2SearchPipeModule,
    Ng2MapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyCdJAwErIy3KmcE_EfHACIvL0Nl1RjhcUo'}),
    LoadersCssModule,
    AngularMultiSelectModule,
    UsuarioRoutingModule
  ],
  declarations: [
    UsuarioComponent,
    DashboardComponent,
    LoaderComponent,
    PerfilComponent,
    UbicacionesComponent,
    AlertasComponent,
    AlertasAtendidasComponent,
    EventosComponent,
  ],
  providers: [
    UsersService,
    RolesService,
    PuestosService,
    ModulosService,
    AccesosService,
    AlertasService,
    EventosService,
    EventosUsuariosService,
    UbicacionesService
  ]
})
export class UsuarioModule { }
