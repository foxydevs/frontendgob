import { Component, OnInit } from '@angular/core';
import { UbicacionesService } from "./../../admin/_services/ubicaciones.service";
import { RolesService } from "./../../admin/_services/roles.service";

import { NotificationsService } from 'angular2-notifications';
import { path } from "../../../config.module";

declare var $: any

@Component({
  selector: 'app-ubicaciones',
  templateUrl: './ubicaciones.component.html',
  styleUrls: ['./ubicaciones.component.css']
})
export class UbicacionesComponent implements OnInit {
  title:string="Ubicaciones"
  Table:any
  idRol=+localStorage.getItem('currentRolId');
  userId:any=+localStorage.getItem('currentId');
  Agregar = +localStorage.getItem('permisoAgregar')
  Modificar = +localStorage.getItem('permisoModificar')
  Eliminar = +localStorage.getItem('permisoEliminar')
  Mostrar = +localStorage.getItem('permisoMostrar')
  secondParentCombo:any
  comboParent:any
  selectedData:any
  parentCombo:any
  beginDate:any
  endDate:any
  currentPosition:any
  public rowsOnPage = 10;
  public search:any
  private basePath:string = path.path
  positions:any = []
  lat:any
  lng:any
  constructor(
    private _service: NotificationsService,
    private secondParentService: RolesService,
    private mainService: UbicacionesService
  ) { }

  ngOnInit() {
    let date = new Date();
      let month = date.getMonth()+1;
      let month2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month
      }
      this.beginDate= date.getFullYear()+'-'+month2+'-01'
      month = date.getMonth()+2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month;
      }
      this.endDate=((date.getHours()<10)?'0'+date.getHours():date.getHours())+':'+((date.getMinutes()<10)?'0'+date.getMinutes():date.getMinutes())+':'+((date.getSeconds()<10)?'0'+date.getSeconds():date.getSeconds())
      // this.cargarSecondParentCombo()
      this.cargarSingle(this.userId)
  }

  cargarSecondParentCombo(){
    this.secondParentService.getAll()
                      .then(response => {
                        this.secondParentCombo = response
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
  }

  cargarAll(){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.getAll()
                      .then(response => {
                        this.Table = response
                        $("#editModal .close").click();
                        $("#insertModal .close").click();
                        $('#Loading').css('display','none')
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })
  }
  onMapReady(map) {

  }
  onIdle(event) {
    // console.log('map', event.target);
  }
  onMarkerInit(marker) {
    // console.log('marker', marker);
  }
  onMapClick(event) {
    this.positions = event.latLng;
    let positions1 = event.latLng + '';
    let pos = positions1.replace(')','').replace('(','').split(',')
    this.lat = pos[0]
    this.lng = pos[1]
    event.target.panTo(this.positions);
    // console.log(this.lat+' @ '+this.lng+' @ '+event.latLng+'\n'+pos[0]);

  }

  insert(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.create(formValue)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Rol Ingresado')
                        $('#Loading').css('display','none')
                        $('#insert-form')[0].reset()
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })


  }
  ubicar(){
    if (navigator.geolocation) {
      let pos
      navigator.geolocation.getCurrentPosition(function(position) {
        pos = {
          latitud: position.coords.latitude,
          longitud: position.coords.longitude
        };
      },
      function(error)
      {
      switch(error.code)
        {
        case error.PERMISSION_DENIED:{
          console.log("El usuario denego la petición de geolocalización.")
          break;}
        case error.POSITION_UNAVAILABLE:{
          console.log("Información de localización no disponible.")
          break;}
        case error.TIMEOUT:{
          console.log("La petición para obtener la ubicación del usuario expiró.")
          break;}
        default:{
          console.log("Error desconocido.")
          break;}
        }
      });
      this.currentPosition=pos
    } else {
      // Browser doesn't support Geolocation
    }
    if(this.currentPosition){
      let formValue = {
        longitud: this.currentPosition.longitud,
        latitud: this.currentPosition.latitud,
        usuario: this.userId,
        descripcion: "Ubicacion Enviada",
      }
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.create(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Ubicacion Enviada')
                          $('#Loading').css('display','none')
                          $('#insert-form')[0].reset()
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                          $('#Loading').css('display','none')
                        })
    }else{
      this.createError("No se pudo obtener ubicacion")
    }
  }
  cargarSingle(id:number){
    this.mainService.getSingle(id)
                      .then(response => {
                        this.selectedData = response;
                        this.positions.length = 0;
                        response.posiciones.forEach(element => {
                          let pos = new google.maps.LatLng(parseFloat(element.latitud), parseFloat(element.longitud));
                          this.positions.push(pos)
                        });
                        // console.log(this.selectedData);
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
  }

  update(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    //console.log(data)
    this.mainService.update(formValue)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Rol Actualizado exitosamente')
                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })

  }

  delete(id:string){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    if(confirm("¿Desea eliminar el Rol?")){
      this.mainService.delete(id)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Rol Eliminado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                          $('#Loading').css('display','none')
                        })
    }else{
      $('#Loading').css('display','none')
    }

  }

  public options = {
    position: ["bottom", "right"],
    timeOut: 2000,
    lastOnBottom: false,
    animate: "fromLeft",
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
    maxLength: 200
};

create(success) {
     this._service.success('¡Éxito!',success)

}
createError(error) {
     this._service.error('¡Error!',error)

}
}

