import { Component, OnInit } from '@angular/core';
import { AlertasService } from "./../../admin/_services/alertas.service";

import { NotificationsService } from 'angular2-notifications';
declare var $: any
import { path } from "../../../config.module";

declare var $: any

@Component({
  selector: 'app-alertas',
  templateUrl: './alertas.component.html',
  styleUrls: ['./alertas.component.css']
})
export class AlertasComponent implements OnInit {
  title:string="Alertas"
  Table:any
  comboParent:any
  selectedData:any
  parentCombo:any
  beginDate:any
  endDate:any
  userId:any;
  idRol=+localStorage.getItem('currentRolId');
  id11=localStorage.getItem('currentId');
  Agregar = +localStorage.getItem('permisoAgregar')
  Modificar = +localStorage.getItem('permisoModificar')
  Eliminar = +localStorage.getItem('permisoEliminar')
  Mostrar = +localStorage.getItem('permisoMostrar')
  currentPosition:any
  public rowsOnPage = 10;
  public search:any
  private basePath:string = path.path
  positions:any
  lat:any
  lng:any
  constructor(
    private _service: NotificationsService,
    private mainService: AlertasService
  ) { }

  ngOnInit() {
    let date = new Date();
      let month = date.getMonth()+1;
      let month2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month
      }
      this.beginDate= date.getFullYear()+'-'+month2+'-01'
      month = date.getMonth()+2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month;
      }
      this.endDate=((date.getHours()<10)?'0'+date.getHours():date.getHours())+':'+((date.getMinutes()<10)?'0'+date.getMinutes():date.getMinutes())+':'+((date.getSeconds()<10)?'0'+date.getSeconds():date.getSeconds())
      this.cargarAll()
  }

  cargarAll(){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.getAllMine(+this.id11)
                      .then(response => {
                        this.Table = response
                        $("#editModal .close").click();
                        $("#insertModal .close").click();
                        $('#Loading').css('display','none')
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })
  }
  onMapReady(map) {

  }
  onIdle(event) {
    // console.log('map', event.target);
  }
  onMarkerInit(marker) {
    // console.log('marker', marker);
  }
  onMapClick(event) {
    this.positions = event.latLng;
    let positions1 = event.latLng + '';
    let pos = positions1.replace(')','').replace('(','').split(',')
    this.lat = pos[0]
    this.lng = pos[1]
    event.target.panTo(this.positions);
    // console.log(this.lat+' @ '+this.lng+' @ '+event.latLng+'\n'+pos[0]);

  }
  ubicar(){
    if (navigator.geolocation) {
      let pos
      navigator.geolocation.getCurrentPosition(function(position) {
        pos = {
          latitud: position.coords.latitude,
          longitud: position.coords.longitude
        };
      },
      function(error)
      {
      switch(error.code)
        {
        case error.PERMISSION_DENIED:{
          console.log("El usuario denego la petición de geolocalización.")
          break;}
        case error.POSITION_UNAVAILABLE:{
          console.log("Información de localización no disponible.")
          break;}
        case error.TIMEOUT:{
          console.log("La petición para obtener la ubicación del usuario expiró.")
          break;}
        default:{
          console.log("Error desconocido.")
          break;}
        }
      });
      this.currentPosition=pos
    } else {
      // Browser doesn't support Geolocation
    }
    if(this.currentPosition){
      let formValue = {
        longitud: this.currentPosition.longitud,
        latitud: this.currentPosition.latitud,
        usuario: this.userId,
        descripcion: "Ubicacion Enviada",
      }
      $('#Loading').css('display','block')
      $('#Loading').addClass('in')
      this.mainService.create(formValue)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Ubicacion Enviada')
                          $('#Loading').css('display','none')
                          $('#insert-form')[0].reset()
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                          $('#Loading').css('display','none')
                        })
    }else{
      this.createError("No se pudo obtener ubicacion")
    }
  }
  insert(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.mainService.create(formValue)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Rol Ingresado')
                        $('#Loading').css('display','none')
                        $('#insert-form')[0].reset()
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })


  }

  cargarSingle(id:number){
    this.mainService.getSingle(id)
                      .then(response => {
                        this.selectedData = response;
                        let dat = response.fecha.split(' ')
                        this.selectedData.fecha = dat[0]
                        this.selectedData.hora = dat[1]
                        this.positions = new google.maps.LatLng(parseFloat(response.latitud), parseFloat(response.longitud));
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
  }

  update(formValue:any,estado:number){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    //console.log(data)
    let data = {
      id: formValue,
      estado: estado
    }
    this.mainService.update(data)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Rol Actualizado exitosamente')
                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })

  }

  delete(id:string){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    if(confirm("¿Desea eliminar el Rol?")){
      this.mainService.delete(id)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Rol Eliminado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                          $('#Loading').css('display','none')
                        })
    }else{
      $('#Loading').css('display','none')
    }

  }

  public options = {
    position: ["bottom", "right"],
    timeOut: 2000,
    lastOnBottom: false,
    animate: "fromLeft",
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
    maxLength: 200
};

create(success) {
     this._service.success('¡Éxito!',success)

}
createError(error) {
     this._service.error('¡Error!',error)

}
}

