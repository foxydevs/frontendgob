import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from "./dashboard/dashboard.component";
import { UsuarioComponent } from "./usuario.component";
import { PerfilComponent } from './perfil/perfil.component';
import { EventosComponent } from './eventos/eventos.component';
import { AlertasComponent } from './alertas/alertas.component';
import { AlertasAtendidasComponent } from './alertas-atendidas/alertas-atendidas.component';
import { UbicacionesComponent } from './ubicaciones/ubicaciones.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '', component: UsuarioComponent, children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'perfil', component: PerfilComponent },
    { path: 'alertas', component: AlertasComponent },
    { path: 'alertas-atendidas', component: AlertasAtendidasComponent },
    { path: 'ubicaciones', component: UbicacionesComponent },
    { path: 'eventos', component: EventosComponent },
  ]},
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
