import { Component, OnInit } from '@angular/core';
import { EventosService } from "./../../admin/_services/eventos.service";
import { EventosUsuariosService } from "./../../admin/_services/eventos-usuarios.service";


import { NotificationsService } from 'angular2-notifications';
import { path } from "../../../config.module";

declare var $: any

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css']
})
export class EventosComponent implements OnInit {
  title:string="Eventos"
  Table:any
  comboParent:any
  selectedData:any
  parentCombo:any
  beginDate:any
  endDate:any
  userId:any=+localStorage.getItem('currentId');
  id11:any=+localStorage.getItem('currentId');
  public rowsOnPage = 10;
  public search:any
  private basePath:string = path.path
  positions:any
  lat:any
  lng:any
  constructor(
    private _service: NotificationsService,
    private mainService: EventosService,
    private childService: EventosUsuariosService
  ) { }

  ngOnInit() {
    let date = new Date();
      let month = date.getMonth()+1;
      let month2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month
      }
      this.beginDate= date.getFullYear()+'-'+month2+'-01'
      month = date.getMonth()+2;
      if(month<10){
        month2='0'+month;
      }else{
        month2=month;
      }
      this.endDate=((date.getHours()<10)?'0'+date.getHours():date.getHours())+':'+((date.getMinutes()<10)?'0'+date.getMinutes():date.getMinutes())+':'+((date.getSeconds()<10)?'0'+date.getSeconds():date.getSeconds())
      this.cargarAll()
  }
  ingresar(formValue:any,like:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.userId=+localStorage.getItem('currentId');
    formValue.usuario = this.userId
    formValue.evento = formValue.id
    formValue.estado = like
    // console.log(formValue);
    this.childService.createAgain(formValue)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Dato Ingresado')
                        $('#Loading').css('display','none')
                        $('#insert-form')[0].reset()
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })
  }

  cargarAll(){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    this.userId=+localStorage.getItem('currentId');
    this.mainService.getAll()
                      .then(response => {
                        this.Table = response
                        $("#editModal .close").click();
                        $("#insertModal .close").click();
                        $('#Loading').css('display','none')
                        console.clear
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })
  }
  onMapReady(map) {

  }
  onIdle(event) {
    // console.log('map', event.target);
  }
  onMarkerInit(marker) {
    // console.log('marker', marker);
  }
  onMapClick(event) {
    this.positions = event.latLng;
    let positions1 = event.latLng + '';
    let pos = positions1.replace(')','').replace('(','').split(',')
    this.lat = pos[0]
    this.lng = pos[1]
    event.target.panTo(this.positions);
    // console.log(this.lat+' @ '+this.lng+' @ '+event.latLng+'\n'+pos[0]);

  }
  subirImagenes(archivo,form,id){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    var archivos=archivo.srcElement.files;
    let url = `${this.basePath}/api/eventos/${form.id}/upload/avatar`

    var i=0;
    var size=archivos[i].size;
    var type=archivos[i].type;
        if(size<(2*(1024*1024))){
          if(type=="image/png" || type=="image/jpeg" || type=="image/jpg"){
        $("#"+id).upload(url,
            {
              avatar: archivos[i]
          },
          function(respuesta)
          {
            $('#imgAvatar').attr("src",'')
            $('#imgAvatar').attr("src",respuesta.picture)
            $('#Loading').css('display','none')
            $("#"+id).val('')
            $("#barra_de_progreso").val(0)
          },
          function(progreso, valor)
          {

            $("#barra_de_progreso").val(valor);
          }
        );
          }else{
            this.createError("El tipo de imagen no es valido")
            $('#Loading').css('display','none')
          }
      }else{
        this.createError("La imagen es demaciado grande")
        $('#Loading').css('display','none')
      }
  }
  insert(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    formValue.latitud = this.lat
    formValue.longitud = this.lng
    this.mainService.create(formValue)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Rol Ingresado')
                        $('#Loading').css('display','none')
                        $('#insert-form')[0].reset()
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })


  }

  cargarSingle(id:number){
    this.childService.getSingle(id)
                      .then(response => {
                        this.selectedData = response;
                        this.positions = new google.maps.LatLng(parseFloat(response.latitud), parseFloat(response.longitud));
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                      })
  }

  update(formValue:any){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    formValue.latitud = this.lat
    formValue.longitud = this.lng
    //console.log(data)
    this.mainService.update(formValue)
                      .then(response => {
                        this.cargarAll()
                        console.clear
                        this.create('Rol Actualizado exitosamente')
                        $('#Loading').css('display','none')
                      }).catch(error => {
                        console.clear
                        this.createError(error)
                        $('#Loading').css('display','none')
                      })

  }

  delete(id:string){
    $('#Loading').css('display','block')
    $('#Loading').addClass('in')
    if(confirm("¿Desea eliminar el Rol?")){
      this.mainService.delete(id)
                        .then(response => {
                          this.cargarAll()
                          console.clear
                          this.create('Rol Eliminado exitosamente')
                          $('#Loading').css('display','none')
                        }).catch(error => {
                          console.clear
                          this.createError(error)
                          $('#Loading').css('display','none')
                        })
    }else{
      $('#Loading').css('display','none')
    }

  }

  public options = {
    position: ["bottom", "right"],
    timeOut: 2000,
    lastOnBottom: false,
    animate: "fromLeft",
    showProgressBar: false,
    pauseOnHover: true,
    clickToClose: true,
    maxLength: 200
};

create(success) {
     this._service.success('¡Éxito!',success)

}
createError(error) {
     this._service.error('¡Error!',error)

}
}

