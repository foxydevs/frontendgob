import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertasAtendidasComponent } from './alertas-atendidas.component';

describe('AlertasAtendidasComponent', () => {
  let component: AlertasAtendidasComponent;
  let fixture: ComponentFixture<AlertasAtendidasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertasAtendidasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertasAtendidasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
