import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DataTableModule } from "angular2-datatable";
import { SimpleNotificationsModule } from 'angular2-notifications';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { LoadersCssModule } from 'angular2-loaders-css';
import { ChartsModule } from 'ng2-charts';
import { AngularMultiSelectModule } from 'angular2-multiselect-checkbox-dropdown/angular2-multiselect-dropdown';
import { Ng2MapModule} from 'ng2-map';

import { AdminRoutingModule } from './admin.routing';

import { UsersService } from "./_services/users.service";
import { RolesService } from "./_services/roles.service";
import { PuestosService } from "./_services/puestos.service";
import { ModulosService } from "./_services/modulos.service";
import { AccesosService } from "./_services/accesos.service";
import { AlertasService } from "./_services/alertas.service";
import { EventosService } from "./_services/eventos.service";
import { EventosUsuariosService } from "./_services/eventos-usuarios.service";
import { UbicacionesService } from "./_services/ubicaciones.service";

import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';
import { LoaderComponent } from './loader/loader.component';
import { RolesComponent } from './roles/roles.component';
import { PuestosComponent } from './puestos/puestos.component';
import { PerfilComponent } from './perfil/perfil.component';
import { ModulosComponent } from './modulos/modulos.component';
import { EventosComponent } from './eventos/eventos.component';
import { AlertasComponent } from './alertas/alertas.component';
import { UbicacionesComponent } from './ubicaciones/ubicaciones.component';
import { EventosUsuariosComponent } from './eventos-usuarios/eventos-usuarios.component';
import { AlertasAtendidasComponent } from './alertas-atendidas/alertas-atendidas.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    ChartsModule,
    SimpleNotificationsModule.forRoot(),
    Ng2SearchPipeModule,
    Ng2MapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyCdJAwErIy3KmcE_EfHACIvL0Nl1RjhcUo'}),
    LoadersCssModule,
    AngularMultiSelectModule,
    AdminRoutingModule
  ],
  declarations: [
    AdminComponent,
    DashboardComponent,
    UsuariosComponent,
    EstadisticasComponent,
    LoaderComponent,
    RolesComponent,
    PuestosComponent,
    PerfilComponent,
    ModulosComponent,
    EventosComponent,
    AlertasComponent,
    UbicacionesComponent,
    EventosUsuariosComponent,
    AlertasAtendidasComponent
  ],
  providers: [
    UsersService,
    RolesService,
    PuestosService,
    ModulosService,
    AccesosService,
    AlertasService,
    EventosService,
    EventosUsuariosService,
    UbicacionesService
  ]
})
export class AdminModule { }
