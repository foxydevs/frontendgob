import { TestBed, inject } from '@angular/core/testing';

import { EventosUsuariosService } from './eventos-usuarios.service';

describe('EventosUsuariosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EventosUsuariosService]
    });
  });

  it('should be created', inject([EventosUsuariosService], (service: EventosUsuariosService) => {
    expect(service).toBeTruthy();
  }));
});
