import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from "./dashboard/dashboard.component";
import { AdminComponent } from "./admin.component";
import { UsuariosComponent } from './usuarios/usuarios.component';
import { RolesComponent } from './roles/roles.component';
import { PuestosComponent } from './puestos/puestos.component';
import { PerfilComponent } from './perfil/perfil.component';
import { ModulosComponent } from './modulos/modulos.component';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';
import { EventosComponent } from './eventos/eventos.component';
import { AlertasComponent } from './alertas/alertas.component';
import { AlertasAtendidasComponent } from './alertas-atendidas/alertas-atendidas.component';
import { UbicacionesComponent } from './ubicaciones/ubicaciones.component';
import { EventosUsuariosComponent } from './eventos-usuarios/eventos-usuarios.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '', component: AdminComponent, children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'usuarios', component: UsuariosComponent },
    { path: 'roles', component: RolesComponent },
    { path: 'puestos', component: PuestosComponent },
    { path: 'perfil', component: PerfilComponent },
    { path: 'modulos', component: ModulosComponent },
    { path: 'eventos', component: EventosComponent },
    { path: 'alertas', component: AlertasComponent },
    { path: 'alertas-atendidas', component: AlertasAtendidasComponent },
    { path: 'ubicaciones', component: UbicacionesComponent },
    { path: 'eventos-usuarios', component: EventosUsuariosComponent },
    { path: 'estadistica', component: EstadisticasComponent },
  ]},
  { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
